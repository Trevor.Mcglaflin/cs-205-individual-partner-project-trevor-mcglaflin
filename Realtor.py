from datetime import datetime
import csv


class Realtor:
    def __init__(self, name, phone, email, password, salary, commission_rate, hire_date, houses):
        self.__name = name
        self.__phone = phone
        self.__email = email
        self.__password = password
        self.__salary = salary
        self.__commission_rate = commission_rate
        self.__hire_date = hire_date
        self.__houses = houses

    # getter methods
    def get_name(self):
        return self.__name

    def get_phone(self):
        return self.__phone

    def get_phone(self):
        return self.__phone

    def get_email(self):
        return self.__email

    def get_password(self):
        return self.__password

    def get_salary(self):
        return self.__salary

    def get_commission_rate(self):
        return self.__commission_rate

    def get_hire_date(self):
        return self.__hire_date

    def get_houses(self):
        return self.__houses

    # setter methods
    def set_name(self, name):
        self.__name = name

    def set_phone(self, phone):
        self.__phone = phone

    def set_email(self, email):
        self.__email = email

    def set_password(self, password):
        self.__password = password

    def set_houses(self, houses):
        self.__houses = houses

    def set_salary(self, salary):
        self.__salary = salary

    def set_commission_rate(self, commission_rate):
        self.__commission_rate = commission_rate

    def set_hire_date(self, hire_date):
        self.__hire_date = hire_date

    # if house address already exists, do not add duplicate, but update info
    def add_house(self, house_to_add):
        address = house_to_add.get_address().lower()
        found_house = None
        for house in self.__houses:
            if house.get_address().lower() == address:
                found_house = house
        if found_house is None:
            self.__houses.append(house_to_add)
        else:
            self.__houses.remove(found_house)
            self.__houses.append(house_to_add)

    # finds house based on address
    def find_house(self, address):
        for house in self.__houses:
            if house.get_address().lower() == address.lower():
                return house
        return None

    # if no parameters are given it will return all houses the realtor has ever sold
    # if one parameter is given, it will return the houses sold since that date to the current date
    # if two parameters are given, it will return the houses sold from date1 to date2
    def get_houses_sold(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = self.__hire_date
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")

        houses_sold = []
        for house in self.__houses:
            if house.get_date_sold() is not None:
                if since_date <= house.get_date_sold() <= to_date:
                    houses_sold.append(house)
        return houses_sold

    # will return how much revenue the realtor has sold
    def get_revenue(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = self.__hire_date
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        # start by getting a list of all houses sold since the given date
        houses_sold_since_date = self.get_houses_sold(since_date, to_date)

        # now sum up all the revenue from those houses
        revenue_sum = 0
        for house in houses_sold_since_date:
            revenue_sum += house.get_market_price()
        return revenue_sum

    # will return the commission that the realtor has earned since the given date
    def get_commission(self, since_date=None, to_date=None):
        revenue = self.get_revenue(since_date, to_date)
        return revenue * self.__commission_rate

    # returns how much salary the realtor has earned during specified period
    def get_periodic_salary(self, since_date=None, to_date=None):
        # convert dates to datetime objects so that we can find number of days between them
        if since_date is None:
            since_date = datetime.strptime(self.__hire_date, '%Y-%m-%d')
        else:
            since_date = datetime.strptime(since_date, '%Y-%m-%d')
        if to_date is None:
            to_date = datetime.now()
        else:
            to_date = datetime.strptime(to_date, '%Y-%m-%d')
        days = (to_date - since_date).days
        num_years = days/365
        return num_years * self.__salary

    # add commission and periodic salary to get income
    def get_income(self, since_date=None, to_date=None):
        return self.get_commission(since_date, to_date) + self.get_periodic_salary(since_date, to_date)

    def get_houses_to_sell(self):
        houses_to_sell = []
        for house in self.__houses:
            if house.get_date_sold() is None:
                houses_to_sell.append(house)
        return houses_to_sell

    def get_average_sell_time(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        # start by getting a list of all houses sold since the given date
        houses_sold_since_date = self.get_houses_sold(since_date, to_date)

        # sum up average length listed
        acc_sell_time = 0
        for house in houses_sold_since_date:
            acc_sell_time += house.length_listed()

        # if no houses have been sold return 1000 days (to symbolize a very high amount of days)
        if len(houses_sold_since_date) == 0:
            return 1000
        else:
            return acc_sell_time/len(houses_sold_since_date)

    def to_string(self, since_date=None, to_date=None):
        fit_name = self.__name.ljust(25)
        fit_phone = self.__phone.rjust(15)
        fit_revenue = ("$" + "{:,}".format(self.get_revenue(since_date, to_date))).rjust(13)
        fit_average_sell_time = str(round(self.get_average_sell_time(since_date, to_date), 2)).rjust(18)
        fit_houses_sold = str(len(self.get_houses_sold(since_date, to_date))).rjust(18)
        return fit_name + fit_phone + fit_revenue + fit_average_sell_time + fit_houses_sold

    def show_income_report(self, since_date=None, to_date=None):
        print("**************************** Income Report ***********************************")
        print("Commission Earned".ljust(20) + ("$" + str(round(self.get_commission(since_date, to_date), 2))).rjust(15))
        print("Salary Earned".ljust(20) + ("$" + str(round(self.get_periodic_salary(since_date, to_date), 2))).rjust(15))
        print("-----------------------------------------------------------------------------")
        print("Total Income Earned".ljust(20) + ("$" + str(round(self.get_income(since_date, to_date), 2))).rjust(15))
        print("********************************************************************************")


    @staticmethod
    def get_realtors_from_file(file_name):
        realtors = []
        try:
            with open(file_name) as file:
                reader = csv.reader(file, delimiter=',')
                line_count = 0
                for row in reader:
                    if line_count == 0:
                        line_count += 1
                    else:
                        realtors.append(Realtor(row[0], row[1], row[2], row[3], int(row[4]), float(row[5]), row[6], []))
                        line_count += 1
            return realtors
        except FileNotFoundError:
            return []






