Important Assumptions/facts/flaws about this system:

- when a house is added, it must immediately be assigned a realtor

- it does not handle the "buy-side", it only focuses on the "sell-side" of real estate,
therefore, when we calculate profits, we do not factor in the cost of the house, only commission 
  expenses and salaries
  
- it is assumed that the primary key for each house is its address, and we will use it to uniquely
identify each house 
  
- it is assumed that the primary key for each realtor is their email, and we will use it to
uniquely identify each realtor
  
- when you update a realtors commission rate or salary, the program will retrospectively change the 
realtors salary or commission rate in all previous periods. For instance, if you change a realtors
 salary from 100000 to 200000 on september 15, 2021 and you go to calculate profit, it will not know
  the date that the employees salary changed, and will not factor the previous salary into the calculation.
  It would have been difficult to do this because you would have had to keep a record of all salary changes
  somewhere
  
