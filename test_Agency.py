import unittest
from Agency import Agency
from Realtor import Realtor
from House import House
from datetime import datetime


class TestAgency(unittest.TestCase):
    def setUp(self):
        # house objects are needed because realtors have houses assigned to them
        self.house1 = House(500000, "406 North Street", "05401", 4, 1.5, 1300, "2021-02-22", None)
        self.house2 = House(400000, "58 Buell Street", "05401", 3, 1, 1250, "2020-07-07", "2021-03-12")
        self.house3 = House(200000, "1 Main Street", "-05449", -3, -1, -1250, "2021-03-12", "2020-07-07")
        self.house4 = House(325000, "1 South Union Street", "05401", 3, 1, 1000, "2020-05-05", None)
        self.house5 = House(1000000, "1 South Union Street", "05401", 3, 1, 1000, "2020-05-05", "2020-07-05")

        # create realtor objects
        self.realtor1 = Realtor("Trevor McGlaflin", "802-324-7275", "tmcglaflin@realtor.com", "skiing123", 150000, 0.25, "2020-01-01", [self.house1, self.house2])
        self.realtor2 = Realtor("Jason Hibbeler", "802-xxx-xxxx", "jhib@realtor.com", "softwareiscool", 250000, 0.29, "2020-01-01", [self.house3, self.house4])
        self.realtor3 = Realtor("Jackie Horton", "802-xxx-xxxx", "jhorton@realtor.com", "javaizoop", 150000, 0.25, "2020-01-01", [self.house5])

        # create an agency object
        # name, phone, email, address, realtors, houses)
        self.agency = Agency("McGlaflin & Co", "802-425-7625", "mcglaflin@realestate.com", "5 Night Run Road", "05445", [self.realtor1, self.realtor2])

    def test_name(self):
        self.assertEqual(self.agency.get_name(), "McGlaflin & Co")
        self.agency.set_name("McGlaflin & Sons")
        self.assertEqual(self.agency.get_name(), "McGlaflin & Sons")

    def test_phone(self):
        self.assertEqual(self.agency.get_phone(), "802-425-7625")
        self.agency.set_phone("802-324-7275")
        self.assertEqual(self.agency.get_phone(), "802-324-7275")

    def test_email(self):
        self.assertEqual(self.agency.get_email(), "mcglaflin@realestate.com")
        self.agency.set_email("mcglaflin@realtors.net")
        self.assertEqual(self.agency.get_email(), "mcglaflin@realtors.net")

    def test_address(self):
        self.assertEqual(self.agency.get_address(), "5 Night Run Road")
        self.agency.set_address("406 North Street")
        self.assertEqual(self.agency.get_address(), "406 North Street")

    def test_zip_code(self):
        self.assertEqual(self.agency.get_zip_code(), "05445")
        self.agency.set_zip_code("05401")
        self.assertEqual(self.agency.get_zip_code(), "05401")

    def test_realtors(self):
        self.assertEqual(self.agency.get_realtors(), [self.realtor1, self.realtor2])
        self.agency.set_realtors([self.realtor1])
        self.assertEqual(self.agency.get_realtors(), [self.realtor1])

    def test_get_houses(self):
        self.assertEqual(self.agency.get_houses(), [self.house1, self.house2, self.house3, self.house4])

    def test_houses_for_sale(self):
        self.assertEqual(self.agency.get_houses_for_sale(), [self.house1, self.house4])

    def test_get_houses_sold(self):
        self.assertEqual(self.agency.get_houses_sold(), [self.house2, self.house3])
        self.assertEqual(self.agency.get_houses_sold("2021-01-01"), [self.house2])
        self.assertEqual(self.agency.get_houses_sold("2020-07-06", "2020-07-08"), [self.house3])

    def test_get_revenue(self):
        self.assertEqual(self.agency.get_revenue(), 600000)
        self.assertEqual(self.agency.get_revenue("2021-01-01"), 400000)
        self.assertEqual(self.agency.get_revenue("2020-07-06", "2020-07-08"), 200000)

    def test_get_commission_expense(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # test all time commission expense
        self.assertEqual(self.agency.get_commission_expense(), 408000)

        # test commission expense since certain date
        self.assertEqual(self.agency.get_commission_expense("2020-07-06"), 158000)

        # test commission expense between dates
        self.assertEqual(self.agency.get_commission_expense("2020-07-01", "2020-07-06"), 250000)

    def test_get_salary_expense(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # test all time salary expense
        realtor1_sal = self.realtor1.get_periodic_salary()
        realtor2_sal = self.realtor1.get_periodic_salary()
        realtor3_sal = self.realtor3.get_periodic_salary()
        self.assertTrue(self.agency.get_salary_expense(), realtor1_sal + realtor2_sal + realtor3_sal)

        # test salary expense since a certain date
        realtor1_sal = self.realtor1.get_periodic_salary("2020-07-06")
        realtor2_sal = self.realtor1.get_periodic_salary("2020-07-06")
        realtor3_sal = self.realtor3.get_periodic_salary("2020-07-06")
        self.assertTrue(self.agency.get_salary_expense("2020-07-06"), realtor1_sal + realtor2_sal + realtor3_sal)

        # test salary expense between two dates
        realtor1_sal = self.realtor1.get_periodic_salary("2020-07-01", "2020-09-01")
        realtor2_sal = self.realtor1.get_periodic_salary("2020-07-01", "2020-09-01")
        realtor3_sal = self.realtor3.get_periodic_salary("2020-07-01", "2020-09-01")
        self.assertTrue(self.agency.get_salary_expense("2020-07-01", "2020-09-01"), realtor1_sal + realtor2_sal + realtor3_sal)

    def test_get_total_selling_expense(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # test all time selling expense
        realtor1_income = self.realtor1.get_income()
        realtor2_income = self.realtor1.get_income()
        realtor3_income = self.realtor3.get_income()
        self.assertTrue(self.agency.get_total_selling_expense(), realtor1_income + realtor2_income + realtor3_income)

        # test salary expense since a certain date
        realtor1_income = self.realtor1.get_income("2020-07-06")
        realtor2_income = self.realtor1.get_income("2020-07-06")
        realtor3_income = self.realtor3.get_income("2020-07-06")
        self.assertTrue(self.agency.get_total_selling_expense("2020-07-06"), realtor1_income + realtor2_income + realtor3_income)

        # test salary expense between two dates
        realtor1_income = self.realtor1.get_income("2020-07-01", "2020-09-01")
        realtor2_income = self.realtor1.get_income("2020-07-01", "2020-09-01")
        realtor3_income = self.realtor3.get_income("2020-07-01", "2020-09-01")
        self.assertTrue(self.agency.get_total_selling_expense("2020-07-01", "2020-09-01"), realtor1_income + realtor2_income + realtor3_income)

    def test_gross_income(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # test all time gross income
        realtor1_gross = self.realtor1.get_revenue() - self.realtor1.get_income()
        realtor2_gross = self.realtor2.get_revenue() - self.realtor2.get_income()
        realtor3_gross = self.realtor3.get_revenue() - self.realtor3.get_income()
        self.assertEqual(round(self.agency.get_gross_income()), round(realtor1_gross + realtor2_gross + realtor3_gross))

        # test gross income since a certain date
        realtor1_gross = self.realtor1.get_revenue("2020-07-06") - self.realtor1.get_income("2020-07-06")
        realtor2_gross = self.realtor2.get_revenue("2020-07-06") - self.realtor2.get_income("2020-07-06")
        realtor3_gross = self.realtor3.get_revenue("2020-07-06") - self.realtor3.get_income("2020-07-06")
        self.assertEqual(round(self.agency.get_gross_income("2020-07-06")), round(realtor1_gross + realtor2_gross + realtor3_gross))

        # test gross income between two dates
        realtor1_gross = self.realtor1.get_revenue("2020-07-01", "2020-09-01") - self.realtor1.get_income("2020-07-01", "2020-09-01")
        realtor2_gross = self.realtor2.get_revenue("2020-07-01", "2020-09-01") - self.realtor2.get_income("2020-07-01", "2020-09-01")
        realtor3_gross = self.realtor3.get_revenue("2020-07-01", "2020-09-01") - self.realtor3.get_income("2020-07-01", "2020-09-01")
        self.assertEqual(round(self.agency.get_gross_income("2020-07-01", "2020-09-01")), round(realtor1_gross + realtor2_gross + realtor3_gross))

    # this test should fail
    def test_gross_income_incorrect(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # test all time gross income
        realtor1_gross = self.realtor1.get_revenue() - self.realtor1.get_income()
        realtor2_gross = self.realtor2.get_revenue() - self.realtor2.get_income()
        realtor3_gross = self.realtor3.get_revenue() - self.realtor3.get_income()
        self.assertEqual(round(self.agency.get_gross_income_incorrect()), round(realtor1_gross + realtor2_gross + realtor3_gross))

        # test gross income since a certain date
        realtor1_gross = self.realtor1.get_revenue("2020-07-06") - self.realtor1.get_income("2020-07-06")
        realtor2_gross = self.realtor2.get_revenue("2020-07-06") - self.realtor2.get_income("2020-07-06")
        realtor3_gross = self.realtor3.get_revenue("2020-07-06") - self.realtor3.get_income("2020-07-06")
        self.assertEqual(round(self.agency.get_gross_income_incorrect("2020-07-06")), round(realtor1_gross + realtor2_gross + realtor3_gross))

        # test gross income between two dates
        realtor1_gross = self.realtor1.get_revenue("2020-07-01", "2020-09-01") - self.realtor1.get_income("2020-07-01", "2020-09-01")
        realtor2_gross = self.realtor2.get_revenue("2020-07-01", "2020-09-01") - self.realtor2.get_income("2020-07-01", "2020-09-01")
        realtor3_gross = self.realtor3.get_revenue("2020-07-01", "2020-09-01") - self.realtor3.get_income("2020-07-01", "2020-09-01")
        self.assertEqual(round(self.agency.get_gross_income_incorrect("2020-07-01", "2020-09-01")), round(realtor1_gross + realtor2_gross + realtor3_gross))


    def test_add_realtor(self):
        # try to add a realtor that already exists, if a realtor with the same email
        # already exists it should update that realtor's information but not add a duplicate record
        self.agency.add_realtor(Realtor("Trevor McGlaflin", "802-xxx-xxxx", "tmcglaflin@realtor.com", "skiing123", 150000, 0.25, "2020-01-01", [self.house1, self.house2]))
        self.assertEqual(len(self.agency.get_realtors()), 2)
        self.assertEqual(self.agency.get_realtors()[1].get_phone(), "802-xxx-xxxx")

        # now, add a new realtor
        self.agency.add_realtor(self.realtor3)
        self.assertEqual(self.agency.get_realtors()[2], self.realtor3)

    def test_remove_realtor(self):
        # first try to remove a realtor that doesn't exist and check that realtors doesn't change
        self.agency.remove_realtor(Realtor("Lisa Dion", "802-xxx-xxxx", "ldion@uvm.edu", "ilikec++", 200000, 0.2, "2019-05-05", []))
        self.assertEqual(len(self.agency.get_realtors()), 2)
        self.assertEqual(self.agency.get_realtors()[0].get_name(), "Trevor McGlaflin")
        self.assertEqual(self.agency.get_realtors()[1].get_name(), "Jason Hibbeler")

        # now, remove a realtor that actually exists and confirm that it was removed
        self.agency.remove_realtor(Realtor("Trevor McGlaflin", "802-324-7275", "tmcglaflin@realtor.com", "skiing123", 150000, 0.25, "2020-01-01", [self.house1, self.house2]))
        self.assertEqual(len(self.agency.get_realtors()), 1)
        self.assertEqual(self.agency.get_realtors()[0].get_name(), "Jason Hibbeler")

    def test_find_realtor(self):
        self.assertEqual(self.agency.find_realtor("tmcglaflin@realtor.com"), self.realtor1)
        self.assertIsNone(self.agency.find_realtor("notagoodemail@email.com"))

    def test_ranked_realtors(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        # if metric is invalid, rank realtors should return an empty list
        self.assertEqual(self.agency.rank_realtors("notametric", "2020-01-01", "2021-01-01"), [])
        self.assertEqual(self.agency.rank_realtors("notametric", "2020-01-01"), [])
        self.assertEqual(self.agency.rank_realtors("notametric"), [])

        # now, test using different metrics and dates

        # test rank realtors by revenue (since no time is specified, it will show all time)
        ordered_realtors = self.agency.rank_realtors("revenue")
        self.assertEqual(ordered_realtors[0], self.realtor3)
        self.assertEqual(ordered_realtors[1], self.realtor1)
        self.assertEqual(ordered_realtors[2], self.realtor2)

        print("********* Realtors ranked by Revenue All Time *************")
        self.agency.show_realtors(ordered_realtors)
        print("********* Done Showing Realtors ranked by Revenue All Time *************")

        # test rank realtors by average sell time (specify a start date)
        ordered_realtors_by_sell_time = self.agency.rank_realtors("average_sell_time", "2020-09-01")
        self.assertEqual(ordered_realtors_by_sell_time[0], self.realtor1)
        self.assertEqual(ordered_realtors_by_sell_time[1], self.realtor2)
        self.assertEqual(ordered_realtors_by_sell_time[2], self.realtor3)

        print("********* Realtors ranked by Average Sell Time Since 2020-12-01 *************")
        self.agency.show_realtors(ordered_realtors_by_sell_time, "2020-09-01")
        print("********* Done Showing Realtors ranked by Average Sell Time Since 2020-12-01  *************")

        # test rank realtors by number of houses sold (specify a start and end date)
        ordered_realtors_by_num_sold = self.agency.rank_realtors("num_sold", "2020-04-01", "2021-01-01")
        self.assertEqual(ordered_realtors_by_num_sold[0], self.realtor2)
        self.assertEqual(ordered_realtors_by_num_sold[1], self.realtor3)
        self.assertEqual(ordered_realtors_by_num_sold[2], self.realtor1)

        print("********* Realtors ranked by Num Houses Sold From 2020-04-01 to 2021-01-01 *************")
        self.agency.show_realtors(ordered_realtors_by_num_sold, "2020-04-01", "2021-01-01")
        print("********* Done Showing Realtors ranked by Num Houses Sold From 2020-04-01 to 2021-01-01  *************")

    def test_profit_report(self):
        # add a third realtor
        self.agency.add_realtor(self.realtor3)

        print("******************* Showing All Time Profit Report *********************************")
        self.agency.show_profit_report()
        print("******************* Done Showing All Time Profit Report ******************************")


if __name__ == '__main__':
    unittest.main()