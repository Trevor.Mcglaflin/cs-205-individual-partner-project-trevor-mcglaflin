from datetime import datetime
import pandas as pd
import csv
from Realtor import Realtor
from House import House


class Agency:
    def __init__(self, name, phone, email, address, zip_code, realtors):
        self.__name = name
        self.__phone = phone
        self.__email = email
        self.__zip_code = zip_code
        self.__address = address
        self.__realtors = realtors

    # getter methods
    def get_name(self):
        return self.__name

    def get_phone(self):
        return self.__phone

    def get_email(self):
        return self.__email

    def get_address(self):
        return self.__address

    def get_zip_code(self):
        return self.__zip_code

    def get_realtors(self):
        return self.__realtors

    # setter methods
    def set_name(self, name):
        self.__name = name

    def set_phone(self, phone):
        self.__phone = phone

    def set_email(self, email):
        self.__email = email

    def set_address(self, address):
        self.__address = address

    def set_zip_code(self, zip_code):
        self.__zip_code = zip_code

    def set_realtors(self, realtors):
        self.__realtors = realtors

    # other public methods
    def get_houses(self):
        houses = []
        for realtor in self.__realtors:
            for house in realtor.get_houses():
                houses.append(house)
        return houses

    def get_houses_for_sale(self):
        houses = self.get_houses()
        houses_for_sale = []
        for house in houses:
            if house.get_date_sold() is None:
                houses_for_sale.append(house)
        return houses_for_sale

    def get_houses_sold(self, since_date=None, to_date=None):
        if since_date is None:
            since_date = "1900-01-01"
        if to_date is None:
            today = datetime.now()
            to_date = today.strftime("%Y-%m-%d")
        houses_sold = []
        for realtor in self.__realtors:
            houses_sold_by_realtor = realtor.get_houses_sold(since_date, to_date)
            for house in houses_sold_by_realtor:
                houses_sold.append(house)
        return houses_sold

    def get_revenue(self, since_date=None, to_date=None):
        houses_sold = self.get_houses_sold(since_date, to_date)
        revenue = 0
        for house in houses_sold:
            revenue += house.get_market_price()
        return revenue

    def get_commission_expense(self, since_date=None, to_date=None):
        commission_sum = 0
        for realtor in self.__realtors:
            commission_sum += realtor.get_commission(since_date, to_date)
        return commission_sum

    def get_salary_expense(self, since_date=None, to_date=None):
        salary_sum = 0
        for realtor in self.__realtors:
            salary_sum += realtor.get_periodic_salary(since_date, to_date)
        return salary_sum

    def get_total_selling_expense(self, since_date=None, to_date=None):
        selling_expense_sum = 0
        for realtor in self.__realtors:
            selling_expense_sum += realtor.get_periodic_salary(since_date, to_date) + realtor.get_commission(since_date, to_date)
        return selling_expense_sum

    def get_gross_income(self, since_date=None, to_date=None):
        return self.get_revenue(since_date, to_date) - self.get_total_selling_expense(since_date, to_date)

    # this is my incorrectly implemented method
    def get_gross_income_incorrect(self, since_date=None, to_date=None):
        return self.get_revenue(since_date, to_date) + self.get_total_selling_expense(since_date, to_date)

    # this function will add a realtor to the realtors list
    # if realtor email already exists, do not add duplicate, but update info
    def add_realtor(self, realtor_to_add):
        email = realtor_to_add.get_email()
        found_realtor = None
        for realtor in self.__realtors:
            if realtor.get_email() == email:
                found_realtor = realtor

        if found_realtor is None:
            self.__realtors.append(realtor_to_add)
        else:
            self.__realtors.remove(found_realtor)
            self.__realtors.append(realtor_to_add)

    # this will remove realtor from realtor list if it exists in the list (based on pk email)
    def remove_realtor(self, realtor_to_remove):
        email = realtor_to_remove.get_email()
        realtor_to_remove = None
        for realtor in self.__realtors:
            if realtor.get_email() == email:
                realtor_to_remove = realtor
        if realtor_to_remove is not None:
            self.__realtors.remove(realtor_to_remove)

    # finds realtor based on email
    def find_realtor(self, email):
        for realtor in self.__realtors:
            if realtor.get_email() == email:
                return realtor
        return None

    # not very readable but it is what it is
    # this would definitely be refactored in the real world
    def rank_realtors(self, metric, since_date=None, to_date=None):
        # if metric is invalid return an empty list
        if metric != "revenue" and metric != "average_sell_time" and metric != "num_sold":
            return []
        else:
            ordered_realtors = []
            if metric == "revenue":
                for realtor in self.__realtors:
                    inserted = False
                    while not inserted:
                        if len(ordered_realtors) == 0:
                            ordered_realtors.append(realtor)
                            inserted = True
                        elif len(ordered_realtors) == 1:
                            if realtor.get_revenue(since_date, to_date) <= ordered_realtors[0].get_revenue(since_date, to_date):
                                ordered_realtors.append(realtor)
                                inserted = True
                            else:
                                ordered_realtors.insert(0, realtor)
                                inserted = True
                        else:
                            count = 0
                            insert_index = 0
                            found = False
                            for ordered_realtor in ordered_realtors:
                                if realtor.get_revenue(since_date, to_date) > ordered_realtor.get_revenue(since_date, to_date) and not found:
                                    insert_index = count
                                    found = True
                                elif count + 1 == len(ordered_realtors) and not found:
                                    insert_index = count + 1
                                count += 1
                            ordered_realtors.insert(insert_index, realtor)
                            inserted = True

            elif metric == "average_sell_time":
                for realtor in self.__realtors:
                    inserted = False
                    while not inserted:
                        if len(ordered_realtors) == 0:
                            ordered_realtors.append(realtor)
                            inserted = True
                        elif len(ordered_realtors) == 1:
                            if realtor.get_average_sell_time(since_date, to_date) >= ordered_realtors[0].get_average_sell_time(since_date, to_date):
                                ordered_realtors.append(realtor)
                                inserted = True
                            else:
                                ordered_realtors.insert(0, realtor)
                                inserted = True
                        else:
                            count = 0
                            insert_index = 0
                            found = False
                            for ordered_realtor in ordered_realtors:
                                if realtor.get_average_sell_time(since_date, to_date) < ordered_realtor.get_average_sell_time(since_date, to_date) and not found:
                                    insert_index = count
                                    found = True
                                elif count + 1 == len(ordered_realtors) and not found:
                                    insert_index = count + 1
                                count += 1
                            ordered_realtors.insert(insert_index, realtor)
                            inserted = True

            else:
                for realtor in self.__realtors:
                    inserted = False
                    while not inserted:
                        if len(ordered_realtors) == 0:
                            ordered_realtors.append(realtor)
                            inserted = True
                        elif len(ordered_realtors) == 1:
                            if len(realtor.get_houses_sold(since_date, to_date)) <= len(ordered_realtors[0].get_houses_sold(since_date, to_date)):
                                ordered_realtors.append(realtor)
                                inserted = True
                            else:
                                ordered_realtors.insert(0, realtor)
                                inserted = True
                        else:
                            count = 0
                            insert_index = 0
                            found = False
                            for ordered_realtor in ordered_realtors:
                                if len(realtor.get_houses_sold(since_date, to_date)) > len(ordered_realtor.get_houses_sold(since_date, to_date)) and not found:
                                    insert_index = count
                                    found = True
                                elif count + 1 == len(ordered_realtors) and not found:
                                    insert_index = count + 1
                                count += 1
                            ordered_realtors.insert(insert_index, realtor)
                            inserted = True
        return ordered_realtors

    def show_realtors(self, realtors=None, since_date=None, to_date=None):
        if realtors is None:
            realtors = self.__realtors
        print("Name".ljust(25), "Phone Number".rjust(15), "Revenue".rjust(13), "Avg Sell Time(days)".rjust(18), "# of Houses Sold".rjust(18))
        for realtor in realtors:
            print(realtor.to_string(since_date, to_date))

    def show_profit_report(self, since_date=None, to_date=None):
        revenue_label = "Revenue".ljust(25)
        selling_expense_label = "Selling Expenses".ljust(25)
        commission_expense_label = "\tCommission Expense".ljust(25)
        salary_expense_label = "\tSalary Expense".ljust(25)
        gross_income_label = "Gross Income".ljust(25)
        revenue_amount = ("$" + "{:,}".format(round(self.get_revenue(since_date, to_date)), 2)).rjust(13)
        commission_expense_amount = ("$" + "{:,}".format(round(self.get_commission_expense(since_date, to_date)), 2)).rjust(13)
        salary_expense_amount = ("$" + "{:,}".format(round(self.get_salary_expense(since_date, to_date)), 2)).rjust(13)
        gross_income_amount = ("$" + "{:,}".format(round(self.get_gross_income(since_date, to_date)), 2)).rjust(13)

        print("*********Partial Income Statement***********")
        print(revenue_label, revenue_amount)
        print(selling_expense_label)
        print(commission_expense_label, commission_expense_amount)
        print(salary_expense_label, salary_expense_amount)
        print("-------------------------------------------------")
        print(gross_income_label, gross_income_amount)
        print()
        print("NOTE: this gross income does not include cost of acquiring the houses.")















