from Agency import Agency
from House import House
from Realtor import Realtor
import csv
from datetime import datetime


def welcome(agency):
    print("Welcome to " + agency.get_name())


def get_role(realtors):
    role = input("Who are you (owner, realtor, buyer): ")
    role = role.lower()
    valid = validate_user(role, realtors)
    while role != "owner" and role != "realtor" and role != "buyer" or valid == False:
        print("Invalid, please enter choice from options")
        role = input("Who are you (owner, realtor, buyer): ")
        role = role.lower()
        valid = validate_user(role, realtors)

    if role == "realtor":
        return valid
    else:
        return role


def validate_user(role, realtors):
    if role == "owner":
        # retrieve owner email and password from file (look at admin_info.csv to find info if you are grading this)
        with open("admin_info.csv") as file:
            reader = csv.reader(file)
            line_count = 0
            for row in reader:
                if line_count == 0:
                    owner_email = row[0]
                if line_count == 1:
                    owner_password = row[0]
                line_count += 1

            # prompt user for email and password, give them three tries
            count_tries = 2
            email = input("Email: ").lower()
            password = input("Password: ").lower()
            while (email != owner_email or password != owner_password) and count_tries != 0:
                print("Invalid attempt, try again (", count_tries, " attempts left ): ")
                email = input("Email: ").lower()
                password = input("Password: ").lower()
                if email != owner_email or password != owner_password:
                    count_tries -= 1

            # return false if validation is failed, and true if they get it in three tries
            if count_tries == 0:
                print("We're sorry, you ran out of login attempts")
                return False
            else:
                return True

    elif role == "realtor":
        # create dictionary to hold realtor login info and populate it
        realtor_info_dict = {}
        for realtor in realtors:
            realtor_info_dict[realtor.get_email()] = realtor.get_password()

        # now prompt user for email and password (give them three tries)
        count_tries = 2
        email = input("Email: ").lower()
        password = input("Password: ").lower()

        found_email = False
        while count_tries != 0:
            for realtor_info in realtor_info_dict:
                if email == realtor_info:
                    found_email = True
                    if password == realtor_info_dict[email]:
                        # we will return the realtors email instead of a boolean so it can be used to access their account
                        # this probably isn't ideal to have a non-bool return type but it works in this case
                        return realtor_info
                    else:
                        count_tries -= 1
            if not found_email:
                count_tries -= 1
            print("Invalid attempt, try again (", count_tries + 1, " attempts left )")
            email = input("Email: ").lower()
            password = input("Password: ").lower()
        print("We're sorry, you ran out of login attempts")
        return False

    # buyers don't need validation
    else:
        return True


def manage_stuff(agency):
    manager_keep_going = "y"
    while manager_keep_going == "y":
        print("1. Analyze Profitability")
        print("2. Analyze Realtor Performance")
        print("3. Add/Edit Realtor Information")
        choice = input("Choose from above (1-3): ")
        while choice != "1" and choice != "2" and choice != "3":
            choice = str(input("Choose from above (1-3): "))

        if choice == "1":
            since_date = input("Enter start date (YYYY-MM-DD format): ")
            to_date = input("Enter end date (YYYY-MM-DD format): ")
            while not valid_date(since_date) or not valid_date(to_date) or since_date > to_date:
                print("Start and/or end date used improper format, or end date was before start date")
                since_date = input("Enter start date (YYYY-MM-DD format): ")
                to_date = input("Enter end date (YYYY-MM-DD format): ")

            agency.show_profit_report(since_date, to_date)
        elif choice == "2":
            print("1. Based on revenue of houses sold")
            print("2. Based on average sell time (house turnover)")
            print("3. Based on quantity of houses sold")
            choice = input("How would you like to evaluate realtor performance?(1, 2, or 3)")
            while choice != "1" and choice != "2" and choice != "3":
                print("Enter 1, 2 or 3 from choices above")
                choice = input("How would you like to evaluate realtor performance?(1, 2, or 3)")
            print("Specify time period over which you want to evaluate performance")
            since_date = input("Enter start date (YYYY-MM-DD format): ")
            to_date = input("Enter end date (YYYY-MM-DD format): ")
            while not valid_date(since_date) or not valid_date(to_date) or since_date > to_date:
                print("Start and/or end date used improper format, or end date was before start date")
                since_date = input("Enter start date (YYYY-MM-DD format): ")
                to_date = input("Enter end date (YYYY-MM-DD format): ")

            # map the metric number to the actual metric
            if choice == "1":
                metric = "revenue"
            elif choice == "2":
                metric = "average_sell_time"
            else:
                metric = "num_sold"

            # now rank the realtors and display them back to user
            ranked_realtors = agency.rank_realtors(metric, since_date, to_date)
            agency.show_realtors(ranked_realtors, since_date, to_date)
        else:
            email = input("Enter email of realtor you want to update/add: ").lower()
            realtor = agency.find_realtor(email)
            if realtor is not None:
                print("1. Name")
                print("2. Phone Number")
                print("3. Salary")
                print("4. Commission Rate")
                choice = input("What do you want to update? (1-4): ")
                while choice != "1" and choice != "2" and choice != "3" and choice != "4":
                    choice = input("What do you want to update? (1-4): ")

                if choice == "1":
                    name = input("Enter new name: ")
                    realtor.set_name(name)
                    print("Name has been updated to " + name + " , for realtor with email " + email)
                elif choice == "2":
                    phone_number = input("Enter new phone number: ")
                    realtor.set_phone(phone_number)
                    print("Phone number has been updated to " + phone_number + " , for realtor with email " + email)
                elif choice == "3":
                    salary = input("Enter new salary: ")
                    while not salary.isdigit():
                        print("Salary must be numeric")
                        salary = input("Enter new salary: ")
                    realtor.set_salary(int(salary))
                    print("Salary has been updated to $" + salary + " , for realtor with email " + email)
                else:
                    commission_rate = input("Enter new commission rate, out of 100%(ie 25):" )
                    while not commission_rate.isdigit():
                        print("Commission rate must be an integer")
                        commission_rate = input("Enter new commission rate, out of 100%(ie 25):")
                    realtor.set_commission_rate(int(commission_rate)/100)
                    print("Commission rate has been updated to " + commission_rate + "%, for realtor with email " + email)
            else:
                print("We need some more information about this new realtor")
                name = input("Enter name: ")
                phone_number = input("Enter phone number: ")
                password = input("Provide them a login password: ")
                salary = input("Enter their starting salary: ")
                while not salary.isdigit():
                    print("Salary must be numeric")
                    salary = input("Enter their starting salary: ")
                commission_rate = input("Enter their starting commission rate: ")
                while not commission_rate.isdigit():
                    print("Commission rate must be numeric")
                    commission_rate = input("Enter their starting commission rate: ")

                # get date today
                todays_date = datetime.now()
                todays_date = todays_date.strftime("%Y-%m-%d")

                # now, add the realtor to the agency's realtor list assuming today is hire date and they have no houses yet
                agency.add_realtor(Realtor(name, phone_number, email, password, int(salary), int(commission_rate)/100, todays_date, []))

                # display updated realtor list
                print("Here is an updated realtor list")
                agency.show_realtors()

        # prompt user to keep doing manager tasks
        manager_keep_going = input("Continue with manager tasks? (y/n):").lower()


def do_buyer_stuff(agency):
    buyer_keep_going = "y"
    while buyer_keep_going == "y":
        print("1. View Houses on the market")
        print("2. Buy a house")
        choice = input("Choose from above (1 or 2): ")
        while choice != "1" and choice != "2":
            choice = str(input("Choose from above (1 or 2): "))

        if choice == "1":
            # prompt user to help narrow down search
            max_price = input("Enter max of price range: ")
            while not max_price.isdigit():
                max_price = input("Enter max of price range (must be an integer value): ")
            min_price = input("Enter min of price range: ")
            while not min_price.isdigit() and  min_price <= max_price:
                min_price = input("Enter min of price range (must be an integer value < max price): ")

            # now display houses with those conditions met
            houses = agency.get_houses_for_sale()
            houses_to_show = []
            for house in houses:
                if house.get_market_price() >= int(min_price) and house.get_market_price() <= int(max_price):
                    houses_to_show.append(house)
            if len(houses_to_show) != 0:
                print("Address".ljust(25) + "Ask Price".rjust(13) + "# Bedrooms".rjust(15) + "# Bathrooms".rjust(15) + "Square Feet".rjust(12))
                for house in houses_to_show:
                    print(house.to_string())
            else:
                print("Sorry, we found no houses that fit those requirements. Come back another time!")

        else:
            # get all addresses of houses for sale
            addresses = []
            for house in agency.get_houses_for_sale():
                addresses.append(house.get_address().lower())

            # prompt user for address (three tries to enter a valid one)
            # GRADER NOTE: to find a list of all houses currently for sale, you can go
            # to choice 1 (you can only buy a house if you enter a valid address of a house for sale)
            count_tries = 2
            address = input("Enter address of house: ").lower()
            while address not in addresses and count_tries != 0:
                print("Address not found, try again(", count_tries, " tries left)" )
                address = input("Enter address of house: ").lower()
                if address not in addresses:
                    count_tries -= 1

            # if user enters valid address, sell the house with that address
            if address in addresses:
                for house in agency.get_houses_for_sale():
                    if address == house.get_address().lower():
                        print("CONGRATS!! You just spent $", house.get_market_price(), " on a house you have never seen!")
                        # sell house
                        house.sell()

            # display message if user runs out of tries
            if count_tries == 0:
                print("We're sorry, you ran out of tries to enter a valid address")

        # ask buyer if they want to continue
        buyer_keep_going = input("Would you like to continue with buyer tasks?(y/n): ").lower()


def do_realtor_stuff(email, agency):
    realtor_keep_going = "y"
    while realtor_keep_going == "y":
        # get realtor
        realtor = agency.find_realtor(email)

        print("1. Show income report")
        print("2. Look at houses I have for sale")
        print("3. Add/Edit Houses for Sale")
        choice = input("Choose from above(1, 2, or 3): ")
        while choice != "1" and choice != "2" and choice != "3":
            choice = input("Choose from above(1, 2, or 3): ")

        if choice == "1":
            print("Choose time period you want to look at")
            since_date = input("Enter start date (YYYY-MM-DD format): ")
            to_date = input("Enter end date (YYYY-MM-DD format): ")
            while not valid_date(since_date) or not valid_date(to_date) or since_date > to_date:
                print("Start and/or end date used improper format, or end date was before start date")
                since_date = input("Enter start date (YYYY-MM-DD format): ")
                to_date = input("Enter end date (YYYY-MM-DD format): ")

            print("Here is your income report between " + since_date + " and " + to_date)
            realtor.show_income_report(since_date, to_date)

        elif choice == "2":
            print("Here are houses you currently have on the market: ")
            print("Address".ljust(25), "Price".rjust(13), "Num Bedrooms".rjust(15), "Num Bathrooms".rjust(15), "Square Feet".rjust(12))
            houses_to_sell = realtor.get_houses_to_sell()
            for house in houses_to_sell:
                print(house.to_string())

        else:
            address = input("Enter address of house you want to update/add: ").lower()
            house = realtor.find_house(address)
            if house is not None:
                print("1. Market Price")
                print("2. Num Bedrooms")
                print("3. Num Bathrooms")
                print("4. Square Feet")
                choice = input("What do you want to update? (1-4): ")
                while choice != "1" and choice != "2" and choice != "3" and choice != "4":
                    choice = input("What do you want to update? (1-4): ")
                value = input("Change to value: ")
                while not value.isdigit():
                    print("Value must be numeric")
                    value = input("Change to value: ")
                if choice == "1":
                    house.set_market_price(value)
                elif choice == "2":
                    house.set_num_bedrooms(value)
                elif choice == "3":
                    house.set_num_bathrooms(value)
                else:
                    house.set_square_feet(value)
            else:
                print("We need some more info about this new house")
                market_price = input("Enter market price: ")
                while not market_price.isdigit():
                    print("Market price must be numeric")
                    market_price = input("Enter market price: ")
                zip_code = input("Enter zip code: ")
                while not zip_code.isdigit():
                    print("Zip Code must be numeric")
                    zip_code = input("Enter zip code: ")
                num_bedrooms = input("Enter number of bedrooms: ")
                while not num_bedrooms.isdigit():
                    print("Number of bedrooms must be numeric")
                    num_bedrooms = input("Enter number of bedrooms: ")
                num_bathrooms = input("Enter number of bathrooms: ")
                while not num_bathrooms.isdigit():
                    print("Number of bathrooms must be numeric")
                    num_bathrooms = input("Enter number of bathrooms: ")
                square_feet = input("Enter number of square feet: ")
                while not square_feet.isdigit():
                    print("Square feet must be numeric")
                    square_feet = input("Enter number of square feet: ")

                # get date today
                todays_date = datetime.now()
                todays_date = todays_date.strftime("%Y-%m-%d")

                # add this house to realtor's house list
                realtor.add_house(House(int(market_price), address, zip_code, int(num_bedrooms), float(num_bathrooms), int(square_feet), todays_date, None))

                # show updated house list
                print("House has been added to house list, here are all houses you currently have on the market")
                print("Address".ljust(25), "Price".rjust(13), "Num Bedrooms".rjust(15), "Num Bathrooms".rjust(15), "Square Feet".rjust(12))
                for house in realtor.get_houses_to_sell():
                    print(house.to_string())

        # ask realtor if they would like to continue
        realtor_keep_going = input("Would you like to continue with realtor tasks?(y/n): ").lower()


def valid_date(stringdate):
    try:
        datetime.strptime(stringdate, "%Y-%m-%d")
        return True
    except ValueError:
        return False

def main():
    # load data
    realtors = Realtor.get_realtors_from_file("Realtors.csv")
    House.get_houses_from_file("Houses.csv", realtors)

    # create agency object
    agency = Agency("McGlaflin Property Co.", "802-425-7625", "trevor@mcglaflinproperty.com", "1 King Street", "05445", realtors)

    # welcome user
    welcome(agency)

    keep_going = "y"
    while keep_going == "y":
        # ask user what kind of user they are
        role = get_role(realtors)
        if role == "owner":
            manage_stuff(agency)
        elif role == "buyer":
            do_buyer_stuff(agency)
        else:
            # for the realtors, the role from above returned the email of the realtor based
            # on what they used to login. So the 'role' in this argument list is actually the email
            do_realtor_stuff(role, agency)

        # ask user if they would like to keep going
        keep_going = input("Would you like to continue in a new role?(y/n)").lower()
    print("BYE! All memory not stored in the csv files has banished forever.")

if __name__ == "__main__":
    main()
