import unittest
from House import House
from Realtor import Realtor
from datetime import datetime


class TestHouse(unittest.TestCase):
    # called before each test
    def setUp(self):
        self.house1 = House(500000, "406 North Street", "05401", 4, 1.5, 1300, "2021-02-22", None)
        self.house2 = House(400000, "58 Buell Street", "05401", 3, 1, 1250, "2020-07-07", "2021-03-12")
        self.house3 = House(-5, "1 Main Street", "-05449", -3, -1, -1250, "2021-03-12", "2020-07-07")
        self.house4 = House(100000, "1 Pearl Street", "05445", 1, 1, 900, "2020-02-22", "2020-02-25")

    def test_market_price(self):
        # confirm constructor assigned values properly
        self.assertEqual(self.house1.get_market_price(), 500000)
        # if negative value is given in constructor, market price should be set to 0
        self.assertEqual(self.house3.get_market_price(), 0)

        # confirm setter method functionality
        self.house1.set_market_price(450000)
        self.house2.set_market_price(-50)
        self.assertEqual(self.house1.get_market_price(), 450000)
        self.assertEqual(self.house3.get_market_price(), 0)

    def test_address(self):
        # confirm that the address was properly set by constructor
        self.assertEqual(self.house1.get_address(), "406 North Street")

        # confirm that the setter method works
        self.house1.set_address("44 Pearl Street")
        self.assertEqual(self.house1.get_address(), "44 Pearl Street")

    def test_zip_code(self):
        # confirm that the zipcode was properly set by constructor
        self.assertEqual(self.house1.get_zip_code(), "05401")

        # confirm that the setter method works
        self.house1.set_zip_code("05445")
        self.assertEqual(self.house1.get_zip_code(), "05445")

    def test_num_bedrooms(self):
        # confirm tha the number of bedrooms was properly set by the constructor
        self.assertEqual(self.house1.get_num_bedrooms(), 4)
        # if negative value is given in constructor or set_num_bedrooms, it should set it to 0
        self.assertEqual(self.house3.get_num_bedrooms(), 0)

        # confirm that the setter method works as intended
        self.house1.set_num_bedrooms(5)
        self.house3.set_num_bedrooms(-10)
        self.assertEqual(self.house1.get_num_bedrooms(), 5)
        self.assertEqual(self.house3.get_num_bedrooms(), 0)

    def test_num_bathrooms(self):
        # confirm that the constructor properly sets the number of bathrooms
        self.assertEqual(self.house1.get_num_bathrooms(), 1.5)
        # if negative value is given in constructor or set_num_bedrooms, it should set it to 0
        self.assertEqual(self.house3.get_num_bathrooms(), 0)

        # confirm that the setter method works
        self.house1.set_num_bathrooms(2)
        self.house3.set_num_bathrooms(-10)
        self.assertEqual(self.house1.get_num_bathrooms(), 2)
        self.assertEqual(self.house3.get_num_bathrooms(), 0)

    def test_square_feet(self):
        # confirm that square feet was set properly in constructor
        self.assertEqual(self.house1.get_square_feet(), 1300)
        # if negative value was given, should default to 0
        self.assertEqual(self.house3.get_square_feet(), 0)

        # confirm that the setter method works
        self.house1.set_square_feet(1500)
        self.house3.set_square_feet(-10)
        self.assertEqual(self.house1.get_square_feet(), 1500)
        self.assertEqual(self.house3.get_square_feet(), 0)

    def test_date_listed(self):
        # confirm that the date_listed was set properly in the constructor
        self.assertEqual(self.house1.get_date_listed(), "2021-02-22")

        # if the given date listed is more recent than the given date sold
        # the date listed should be set to the date sold (for setter method and constructor)
        self.assertEqual(self.house3.get_date_listed(), "2020-07-07")

        # confirm setter method works as intended
        self.house1.set_date_listed("2021-01-22")
        self.house2.set_date_listed("2021-03-14")
        self.assertEqual(self.house1.get_date_listed(), "2021-01-22")
        self.assertEqual(self.house2.get_date_listed(), "2021-03-12")

    def test_date_sold(self):
        # confirm that the date sold is properly set
        self.assertIsNone(self.house1.get_date_sold())
        self.assertEqual(self.house3.get_date_sold(), "2020-07-07")

        # confirm the setter method works as expected
        # if the user tries to set the date sold to a time after the date listed, it should set it to the date listed
        self.house1.set_date_sold("2021-03-01")
        self.house2.set_date_sold("2020-01-12")
        self.assertEqual(self.house1.get_date_sold(), "2021-03-01")
        self.assertEqual(self.house2.get_date_sold(), "2020-07-07")

    def test_sell(self):
        # this sell method can take 0 or 1 arguments
        # if 0 arguments are given, the date_sold should be set to whatever todays date is
        # if 1 argument is given, the date_sold should be set to the date in the argument
        today = datetime.today()
        formatted_today = today.strftime("%Y-%m-%d")

        # test with no arguments
        self.house1.sell()
        self.assertEqual(self.house1.get_date_sold(), formatted_today)

        # test with one argument
        self.house1.sell("2021-03-22")
        self.assertEqual(self.house1.get_date_sold(), "2021-03-22")

        # test case where given sell date is before date_listed
        self.house1.sell("2016-03-22")
        self.assertEqual(self.house1.get_date_sold(), "2021-02-22")

    def test_length_listed(self):
        # if the house hasn't been sold, it will return the number of days between the date
        # listed and todays date. If the house has been sold it will return the number of days
        # between the date_listed and date_sold

        # test case when there is no sell date
        start = datetime.strptime(self.house1.get_date_listed(), "%Y-%m-%d")
        end = datetime.today()
        self.assertEqual(self.house1.length_listed(), (end-start).days)

        # test case when there is a sell date
        self.assertEqual(self.house4.length_listed(), 3)

    def test_get_houses_from_file(self):
        # this method takes the name of a csv file and a list of realtors as arguments
        # the csv file will contain a fk for each entry which is the realtor's email
        # that is assigned to that house
        # this method will append the houses list for each realtor for each house that they are assigned
        # therefore, we need a list of realtors to start
        realtors = Realtor.get_realtors_from_file("test_realtors.csv")

        # now, load the houses in for each realtor
        House.get_houses_from_file("test_houses.csv", realtors)

        # confirm the length of realtors has not changed
        self.assertEqual(len(realtors), 3)

        # confirm House objects were added properly to each realtor's house list
        self.assertEqual(len(realtors[0].get_houses()), 1)
        self.assertEqual(len(realtors[1].get_houses()), 1)
        self.assertEqual(len(realtors[2].get_houses()), 1)
        self.assertEqual(realtors[0].get_houses()[0].get_address(), "10 Beverly Hills Dr")
        self.assertEqual(realtors[0].get_houses()[0].get_date_sold(), "2020-07-25")
        self.assertIsNone(realtors[2].get_houses()[0].get_date_sold())
        self.assertEqual(realtors[1].get_houses()[0].get_market_price(), 1575000)


















